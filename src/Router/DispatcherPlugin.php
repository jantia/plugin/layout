<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Layout\Router;

//
use Jantia\Plugin\Layout\Response\Layout;
use Tiat\Mvc\Dispatcher as MvcDispatcher;
use Tiat\Mvc\Helper\HeadersHelperTrait;
use Tiat\Mvc\Helper\HeadersHelperTraitInterface;
use Tiat\Router\Exception\RuntimeException;
use Tiat\Router\Response\ResponseFormat;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class DispatcherPlugin extends MvcDispatcher implements HeadersHelperTraitInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use HeadersHelperTrait;
	
	/**
	 * Default shutdown for Controller Response. Return EMPTY if no content.
	 *
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void {
		if(( $c = $this->getControllerInterface() ) !== NULL):
			// Create layout object and set headers from controller response.
			( $l = new Layout($c->getResponseContent(), $c->getResponseFormat() ?? ResponseFormat::EMPTY,
			                  $c->getResponseCode()) );
			
			// Collect headers from Dispatcher and set them to controller response.
			if(! empty($headers = $this->getHeaders())):
				$this->setHeaders($headers, TRUE);
			endif;
			
			// Collect headers from Controller and set them to layout.
			if(! empty($headers = $c->getHeaders())):
				$this->setHeaders($headers, TRUE);
			endif;
			
			// Set all headers to response.
			if(! empty($headers = $this->getHeaders())):
				$l->setHeaders($headers, TRUE);
			endif;
			
			// Run layout and set response content.
			$l->init()->run();
		else:
			throw new RuntimeException("No controller available.");
		endif;
	}
}
