<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Layout\Response;

//
use Laminas\Diactoros\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Tiat\Mvc\Helper\HeadersHelperTrait;
use Tiat\Router\Response\ResponseContent;
use Tiat\Router\Response\ResponseContentInterface;
use Tiat\Router\Response\ResponseFormat;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractLayout implements LayoutInterface, ResponseContentInterface {
	
	/**
	 *
	 */
	use HeadersHelperTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ResponseContent;
	
	/**
	 * @var ResponseInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected ResponseInterface $_responseHandler;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_responseHeaders = [];
	
	/**
	 * @param    mixed                  $responseContent
	 * @param    ResponseFormat|NULL    $responseFormat
	 * @param    int|NULL               $responseCode
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(mixed $responseContent, ResponseFormat $responseFormat = NULL, int $responseCode = NULL) {
		$this->setResponseContent($responseContent, $responseFormat, $responseCode);
	}
	
	/**
	 * @return null|ResponseInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseHandler() : ?ResponseInterface {
		return $this->_responseHandler ?? NULL;
	}
	
	/**
	 * @param    ResponseFormat    $format
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setResponseHandler(ResponseFormat $format) : void;
	
	/**
	 * Set headers from response content. It there is manually added headers then those are added after content headers.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _setHeaders() : void;
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _getResponseHeaders() : array;
	
	/**
	 * @return null|Stream|StreamInterface
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _getBody() : Stream|StreamInterface|null;
}
