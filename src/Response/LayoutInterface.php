<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Layout\Response;

use Psr\Http\Message\ResponseInterface;
use Tiat\Mvc\Helper\HeadersHelperTraitInterface;
use Tiat\Router\Response\ResponseFormat;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface LayoutInterface extends HeadersHelperTraitInterface {
	
	/**
	 * @param    mixed                  $responseContent
	 * @param    ResponseFormat|NULL    $responseFormat
	 * @param    int|NULL               $responseCode
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(mixed $responseContent, ResponseFormat $responseFormat = NULL, int $responseCode = NULL);
	
	/**
	 * @return null|ResponseInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getResponseHandler() : ?ResponseInterface;
	
	/**
	 * @return LayoutInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init() : LayoutInterface;
	
	/**
	 * @return LayoutInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run() : LayoutInterface;
}
