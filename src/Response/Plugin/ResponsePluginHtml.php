<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Layout\Response\Plugin;

//
use Jantia\Plugin\Layout\Render\Tidy;
use Laminas\Diactoros\Stream;
use Psr\Http\Message\StreamInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class ResponsePluginHtml {
	
	/**
	 * @param    StreamInterface    $_stream
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(protected StreamInterface $_stream) {
	
	}
	
	/**
	 * @return Stream
	 * @since   3.0.0 First time introduced.
	 */
	public function run() : Stream {
		//
		if((string)$this->_stream !== ''):
			$tidy = new Tidy((string)$this->_stream, 'utf8');
			
			// Set Default replace for HTML5 doctype declaration because it's not detected as default
			$this->_setDefaultReplace($tidy);
			
			//
			$content = $tidy->run();
			
			// Delete old stream
			$this->_stream->close();
		endif;
		
		// Generate new stream
		$body = new Stream('php://temp', 'wb+');
		$body->write($content ?? '');
		$body->rewind();
		
		//
		return $body;
	}
	
	/**
	 * Sets the default replace values for Tidy object.
	 *
	 * @param    Tidy    $tidy    The Tidy object.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setDefaultReplace(Tidy $tidy) : void {
		# Replace values
		$pattern = ['/<html lang="([^"]+)">/' => '<!DOCTYPE html>' . PHP_EOL . '<html lang="$1">'];
		$search  = ['<html lang="fi">'];
		
		//
		$tidy->setReplaceValues($search, [], $pattern);
	}
}
