<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Layout\Response;

//
use Jantia\Plugin\Layout\Exception\InvalidArgumentException;
use Jantia\Plugin\Layout\Response\Plugin\ResponsePluginHtml;
use JsonException;
use Laminas\Diactoros\Stream;
use Psr\Http\Message\StreamInterface;
use Tiat\Router\Response\Format\CsvResponse;
use Tiat\Router\Response\Format\EmptyResponse;
use Tiat\Router\Response\Format\HtmlResponse;
use Tiat\Router\Response\Format\JsonResponse;
use Tiat\Router\Response\Format\RedirectResponse;
use Tiat\Router\Response\Format\RssResponse;
use Tiat\Router\Response\Format\TextResponse;
use Tiat\Router\Response\Format\XmlResponse;
use Tiat\Router\Response\Format\YamlResponse;
use Tiat\Router\Response\ResponseFormat;

use function header;
use function headers_sent;
use function in_array;
use function is_array;
use function is_string;
use function ob_end_clean;
use function ob_flush;
use function ob_get_level;
use function ob_start;
use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Layout extends AbstractLayout {
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function init() : static {
		if(! empty($content = $this->getResponseContent()) &&
		   ! ( $content instanceof StreamInterface || is_string($content) )):
			// Modify output with custom headers when running in SWR mode (service-worker-render).
			#$this->setResponseContent((new Swr($content))->toString());
		endif;
		
		// Set response handler with content & code
		try {
			$this->_setResponseHandler($this->getResponseFormat());
		} catch(JsonException $e) {
			throw new InvalidArgumentException($e->getMessage());
		}
		
		// Set all headers (including extra handlers)
		$this->_setHeaders();
		
		//
		return $this;
	}
	
	/**
	 * @param    ResponseFormat    $format
	 *
	 * @return void
	 * @throws JsonException
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setResponseHandler(ResponseFormat $format) : void {
		//
		$handler = match ( $format ) {
			ResponseFormat::CSV => new CsvResponse($this->getResponseContent(), $this->getResponseCode()),
			ResponseFormat::EMPTY => new EmptyResponse(204),
			ResponseFormat::HTML => new HtmlResponse($this->getResponseContent(), $this->getResponseCode()),
			ResponseFormat::JSON => new JsonResponse($this->getResponseContent(), $this->getResponseCode()),
			ResponseFormat::REDIRECT => new RedirectResponse($this->getResponseContent(), $this->getResponseCode()),
			ResponseFormat::RSS => new RssResponse($this->getResponseContent(), $this->getResponseCode()),
			ResponseFormat::TEXT => new TextResponse($this->getResponseContent(), $this->getResponseCode()),
			ResponseFormat::XML => new XmlResponse($this->getResponseContent(), $this->getResponseCode()),
			ResponseFormat::YAML => new YamlResponse($this->getResponseContent(), $this->getResponseCode()),
			default => throw new InvalidArgumentException(sprintf("%s response format is not supported.",
			                                                      $format->value))
		};
		
		//
		$this->_responseHandler = $handler;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setHeaders() : void {
		//
		if(( $h = $this->getResponseHandler() ) !== NULL):
			// Get response headers and set them to response.
			$this->_responseHeaders[] =
				'HTTP/' . $h->getProtocolVersion() . ' ' . $h->getStatusCode() . ' ' . $h->getReasonPhrase();
			
			// Get custom headers and set them to response.
			$this->_setHeadersArray($h->getHeaders())->_setHeadersArray($this->getHeaders() ?? []);
		endif;
	}
	
	/**
	 * @param    array    $headers
	 *
	 * @return Layout
	 * @since   3.0.0 First time introduced.
	 */
	private function _setHeadersArray(array $headers) : static {
		//
		if(! empty($headers)):
			foreach($headers as $key => $val):
				if(is_array($val)):
					foreach($val as $val2):
						$this->_responseHeaders[] = $key . ': ' . $val2;
					endforeach;
				elseif(str_contains(':', $val)):
					$this->_responseHeaders[] = $val;
				elseif(! in_array($val, $this->_responseHeaders ?? [], TRUE)):
					$this->_responseHeaders[] = $key . ':' . $val;
				endif;
			endforeach;
			
			// Remove duplicates from headers
			$this->_responseHeaders = array_unique($this->_responseHeaders);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function run() : static {
		// Start buffering the output
		ob_start();
		
		if(! empty($headers = $this->_getResponseHeaders()) && ! headers_sent()):
			foreach($headers as $val):
				header($val);
			endforeach;
		endif;
		
		// Get body
		echo $this->_getBody();
		
		// Flush content
		if(ob_get_level()):
			// Get the contents of the output buffer
			ob_flush();
			
			// Ensure all data has been sent to the client
			flush();
		endif;
		
		// Stop, clean & erase buffer
		ob_end_clean();
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getResponseHeaders() : array {
		return $this->_responseHeaders;
	}
	
	/**
	 * @return null|Stream|StreamInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getBody() : Stream|StreamInterface|null {
		if(( $h = $this->getResponseHandler() ) !== NULL):
			// Get response body.
			$content = $h->getBody();
			
			// Get response body as stream.
			if($this->getResponseFormat() === ResponseFormat::HTML):
				$content = ( new ResponsePluginHtml($content) )->run();
			endif;
		endif;
		
		//
		return $content ?? NULL;
	}
}
