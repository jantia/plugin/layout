<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Layout\Render;

//
use Jantia\Plugin\Layout\Exception\InvalidArgumentException;

use function array_keys;
use function implode;
use function in_array;
use function key;
use function next;
use function sprintf;
use function strtolower;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractTidy implements TidyInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array ENCODING_SUPPORTED = ['ascii', 'latin0', 'latin1', 'raw', 'utf8', 'iso2022', 'mac',
	                                               'win1252', 'ibm858', 'utf16', 'utf16le', 'utf16be', 'big5',
	                                               'shiftjis'];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string ENCODING_DEFAULT = 'utf8';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array CONFIG_DEFAULT = ['doctype' => 'omit', 'indent' => 2, 'output-html' => TRUE,
	                                           'tidy-mark' => FALSE, 'indent-spaces' => 4, 'wrap' => 200,
	                                           'hide-comments' => TRUE, 'markup' => 'yes', 'output-xml' => 'no',
	                                           'input-xml' => 'no', 'show-warnings' => 'yes',
	                                           'numeric-entities' => 'yes', 'quote-marks' => 'yes',
	                                           'quote-nbsp' => 'yes', 'quote-ampersand' => 'no',
	                                           'break-before-br' => 'no', 'uppercase-tags' => 'no',
	                                           'uppercase-attributes' => 'no', 'char-encoding' => 'utf8',
	                                           'output-encoding' => 'utf8',
	                                           'new-inline-tags' => 'audio command datalist embed keygen mark menuitem meter output progress source time video wbr',
	                                           'new-blocklevel-tags' => 'article aside audio bdi canvas details dialog figcaption figure footer header hgroup main menu menuitem nav section source summary template track video',
	                                           'new-empty-tags' => 'command embed keygen source track wbr',];
	
	/**
	 * Content which will be replaced after Tidy has been completed.
	 *
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_replace;
	
	/**
	 * @var array|null[]
	 * @since   3.0.0 First time introduced.
	 */
	protected readonly array $_replaceTemplate;
	
	/**
	 * @var array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_encodingSupported = self::ENCODING_SUPPORTED;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_encoding = 'utf8';
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_configDefault = self::CONFIG_DEFAULT;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_content;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct() {
		$this->_replaceTemplate = ['search' => NULL, 'replace' => NULL, 'pattern' => NULL];
	}
	
	/**
	 * @param    array    $supported
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncodingSupport(array $supported) : static {
		//
		$this->_encodingSupported = $supported;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEncodingSupport() : static {
		//
		$this->_encodingSupported = self::ENCODING_SUPPORTED;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncoding() : string {
		return $this->_encoding;
	}
	
	/**
	 * @param    string    $encoding
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : static {
		//
		if($this->checkEncoding($encoding = strtolower($encoding)) === TRUE):
			$this->_encoding = $encoding;
		else:
			$msg = sprintf("Tidy is not supporting given encoding %s. Possible values are %s.", $encoding,
			               implode(', ', $this->getEncodingSupport()));
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $encoding
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkEncoding(string $encoding) : bool {
		return in_array(strtolower($encoding), $this->getEncodingSupport(), TRUE);
	}
	
	/**
	 * Supported encodings
	 *
	 * @return string[]
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncodingSupport() : array {
		return $this->_encodingSupported;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEncoding() : static {
		//
		$this->_encoding = self::ENCODING_DEFAULT;
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfigDefault() : array {
		return $this->_configDefault;
	}
	
	/**
	 * @param    array    $config
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setConfigDefault(array $config) : static {
		//
		$this->_configDefault = $config;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConfigDefault() : static {
		//
		$this->_configDefault = self::CONFIG_DEFAULT;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getContent() : ?string {
		return $this->_content ?? NULL;
	}
	
	/**
	 * @param    string    $content
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setContent(string $content) : static {
		//
		$this->_content = $content;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetContent() : static {
		//
		unset($this->_content);
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getReplaceValues() : ?array {
		return $this->_replace ?? NULL;
	}
	
	/**
	 * @param    array|string         $search
	 * @param    array|string         $replace
	 * @param    null|string|array    $pattern
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setReplaceValues(array|string $search, array|string $replace, string|array $pattern = NULL) : static {
		//
		[$key1, $key2, $key3] = $this->getReplaceKeys();
		
		//
		$this->_replace[] = [$key1 => $search, $key2 => $replace, $key3 => $pattern];
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	final public function getReplaceKeys() : array {
		//
		$t = array_keys($this->_replaceTemplate);
		
		//
		$key1 = $t[key($t)];
		next($t);
		$key2 = $t[key($t)];
		next($t);
		$key3 = $t[key($t)];
		
		//
		return [$key1, $key2, $key3];
	}
}
