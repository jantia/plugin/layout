<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Layout\Render;

//
use Tidy as TidyPackage;

use function current;
use function implode;
use function is_array;
use function key;
use function preg_replace_callback;
use function str_replace;

/**
 * Tidy is used with HTML content to parse it nice and clean format.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Tidy extends AbstractTidy {
	
	/**
	 * @var TidyPackage
	 * @since   3.0.0 First time introduced.
	 */
	private TidyPackage $_tidyInterface;
	
	/**
	 * @param    string    $content
	 * @param    string    $encoding
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(string $content, string $encoding = 'utf8') {
		//
		parent::__construct();
		
		//
		$this->setContent($content)->setEncoding($encoding);
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function run() : ?string {
		if(( $tidy = $this->_getTidy() ) !== NULL):
			$tidy->parseString($this->getContent(), $this->getConfigDefault(), $this->getEncoding());
			$tidy->cleanRepair();
		endif;
		
		//
		$html    = $tidy->html();
		$content = $html->value;
		
		//
		if(! empty($replace = $this->getReplaceValues())):
			//
			[$key1, $key2, $key3] = $this->getReplaceKeys();
			
			//
			foreach($replace as $val):
				//
				if(! empty($val[$key3])):
					$content = $this->_usePattern($val[$key3], $content);
				endif;
				
				//
				if(! empty($val[$key1]) && ! empty($val[$key2])):
					$content = str_replace($val[$key1], $val[$key2], $content);
				endif;
			endforeach;
		endif;
		
		//
		if(is_array($content)):
			$content = implode('', $content);
		endif;
		
		//
		return (string)$content;
	}
	
	//
	
	/**
	 * @return null|TidyPackage
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getTidy() : ?TidyPackage {
		//
		if(empty($this->_tidyInterface)):
			$this->_setTidy();
		endif;
		
		//
		return $this->_tidyInterface ?? NULL;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setTidy(TidyPackage $tidy = NULL) : static {
		//
		if($tidy !== NULL):
			$this->_tidyInterface = $tidy;
		else:
			$this->_tidyInterface = new TidyPackage();
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    array|string    $pattern
	 * @param    string          $content
	 *
	 * @return null|array|string
	 */
	private function _usePattern(array|string $pattern, string $content) : array|string|null {
		//
		$search = key($pattern);
		$value  = current($pattern);
		
		//
		$result = preg_replace_callback($search, static function ($matches) use ($value) {
			//
			$langCode = $matches[1];
			
			// Define the replacement string using the captured language code
			return str_replace('$1', $langCode, $value);
		},                              $content);
		
		//
		if(! empty($result) || $result !== $content):
			return $result;
		endif;
		
		//
		return $content;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _resetTidy() : static {
		//
		unset($this->_tidyInterface);
		
		//
		return $this;
	}
}
