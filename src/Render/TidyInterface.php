<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Layout
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Layout\Render;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface TidyInterface {
	
	/**
	 * @param    array    $supported
	 *
	 * @return TidyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncodingSupport(array $supported) : TidyInterface;
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEncodingSupport() : TidyInterface;
	
	/**
	 * Supported encodings
	 *
	 * @return string[]
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncodingSupport() : array;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEncoding() : string;
	
	/**
	 * @param    string    $encoding
	 *
	 * @return TidyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEncoding(string $encoding) : TidyInterface;
	
	/**
	 * @return TidyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEncoding() : TidyInterface;
	
	/**
	 * @param    string    $encoding
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkEncoding(string $encoding) : bool;
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConfigDefault() : array;
	
	/**
	 * @param    array    $config
	 *
	 * @return TidyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConfigDefault(array $config) : TidyInterface;
	
	/**
	 * @return TidyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConfigDefault() : TidyInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getContent() : ?string;
	
	/**
	 * @param    string    $content
	 *
	 * @return TidyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setContent(string $content) : TidyInterface;
	
	/**
	 * @return TidyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetContent() : TidyInterface;
	
	/**
	 * Replace content after Tidy has completed (example HTML5)
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getReplaceValues() : ?array;
	
	/**
	 * Replace content after Tidy has completed (example HTML5)
	 *
	 * @param    array|string         $search
	 * @param    array|string         $replace
	 * @param    null|string|array    $pattern
	 *
	 * @return TidyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setReplaceValues(array|string $search, array|string $replace, string|array $pattern = NULL) : TidyInterface;
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getReplaceKeys() : array;
}
